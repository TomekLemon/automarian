$(function () {
    fckconfig_common = {
        skin: 'BootstrapCK-Skin',
        contentsCss: ['/bundles/lscore/front/css/editor.css'],
        bodyClass: 'editor',
        stylesSet: [
            {name: 'Normalny', element: 'p', attributes: {'class': ''}}
        ],
        filebrowserBrowseUrl: '/bundles/lscore/kcfinder-3.12/browse.php?type=files',
        filebrowserImageBrowseUrl: '/bundles/lscore/common/kcfinder-3.12/browse.php?type=images',
        filebrowserFlashBrowseUrl: '/bundles/lscore/common/kcfinder-3.12/browse.php?type=flash',
        filebrowserUploadUrl: '/bundles/lscore/common/kcfinder-3.12/upload.php?type=files',
        filebrowserImageUploadUrl: '/bundles/lscore/common/kcfinder-3.12/upload.php?type=images',
        filebrowserFlashUploadUrl: '/bundles/lscore/common/kcfinder-3.12/upload.php?type=flash'
    };

    fckconfig = jQuery.extend(true, {
        height: '400px',
        width: 'auto'
    }, fckconfig_common);

    $('.wysiwyg').ckeditor(fckconfig);
});