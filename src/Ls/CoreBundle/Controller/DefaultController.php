<?php

namespace Ls\CoreBundle\Controller;

use Ls\CoreBundle\Helper\AdminBlock;
use Ls\CoreBundle\Helper\AdminDashboard;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    protected $containerBuilder;

    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        return $this->render('LsCoreBundle:Default:index.html.twig', array());
    }

    public function adminAction()
    {
        $authorizationChecker = $this->container->get('security.authorization_checker');
        $dashboardConfigName = $authorizationChecker->isGranted('ROLE_ADMIN')
                ? 'ls_core.admin.dashboard'
                : 'ls_core.admin.dashboard_for_client';

        $blocks = $this->container->getParameter($dashboardConfigName);
        $dashboard = new AdminDashboard();

        foreach ($blocks as $block) {
            $parent = new AdminBlock($block['label']);
            $dashboard->addBlock($parent);
            foreach ($block['items'] as $item) {
                $service = $this->container->get($item);
                $service->addToDashboard($parent);
            }
        }

        return $this->render('LsCoreBundle:Default:admin.html.twig', array(
            'dashboard' => $dashboard,
        ));
    }
}
