<?php

namespace Ls\UserBundle\Form;

use Ls\UserBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Count;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class UserType extends AbstractType
{
    private $options;
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->options = $options;

        $builder->add('username', null, array(
            'label' => 'Nazwa konta',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole',
                )),
            ),
        ));
        $builder->add('email', null, array(
            'label' => 'Adres e-mail',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole',
                )),
                new Email(array(
                    'message' => 'Niepoprawny adres e-mail',
                )),
            ),
        ));
        $builder->add('active', null, array(
            'label' => 'Aktywne',
        ));
        $builder->add('roles', ChoiceType::class, array(
            'label' => 'Role',
            'choices' => User::getRolesOptions(),
            'choices_as_values' => true,
            'required' => false,
            'multiple' => true,
            'expanded' => true,
            'placeholder' => false,
            'constraints' => array(
                new Count(array(
                    'min' => 1,
                    'minMessage' => 'Wybierz przynajmniej jedną rolę',
                )),
            ),
        ));
        $builder->add('client', EntityType::class, array(
            'label' => 'Klient',
            'class' => 'LsGasStationBundle:Client',
            'required' => false,
            'query_builder' => function (EntityRepository $er) {
                $query = $er->createQueryBuilder('c')
                        ->select('partial c.{id, name}')
                        ->leftJoin('c.user', 'u')
                        ->where('u.id is null');

                if (!empty($this->options) && !empty($this->options['data'])) {
                    $query = $query
                        ->orWhere('u.id = :currentlyEditedUserId')
                        ->setParameter('currentlyEditedUserId', $this->options['data']->getId());
                }

                return $query;
            },
        ));
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $user = $event->getData();
            $form = $event->getForm();

            if (!$user || null === $user->getId()) {
                $form->add('plain_password', TextType::class, array(
                    'label' => 'Hasło',
                    'constraints' => array(
                        new NotBlank(array(
                            'message' => 'Wypełnij pole',
                        )),
                    ),
                ));
            } else {
                $form->add('plain_password', TextType::class, array(
                    'label' => 'Hasło',
                ));
            }
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\UserBundle\Entity\User',
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'form_admin_user';
    }
}
