<?php

namespace Ls\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Ls\UserBundle\Validators\FreeUserEmail;

class ClientAccountEditType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', null, array(
            'label' => 'Adres e-mail',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole',
                )),
                new Email(array(
                    'message' => 'Niepoprawny adres e-mail',
                )),
                new FreeUserEmail(array(
                    'excludedUserId' => $options['excludedUserId'],
                )),
            ),
        ));

        $builder->add('clientName', null, array(
            'label' => 'Nazwa',
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired(['excludedUserId']);
        $resolver->setAllowedTypes('excludedUserId', 'integer');
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'form_client_account_edit';
    }
}
