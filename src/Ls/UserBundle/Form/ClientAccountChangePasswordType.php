<?php

namespace Ls\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class ClientAccountChangePasswordType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('password', RepeatedType::class, array(
            'type' => PasswordType::class,
            'invalid_message' => 'Hasła muszą być jednakowe.',
            'first_options' => array(
                'required' => true,
                'label' => 'Nowe hasło',
            ),
            'second_options' => array(
                'required' => true,
                'label' => 'Powtórz hasło',
            ),
            'options' => array(
                'attr' => array('class' => 'password-field'),
                'constraints' => array(
                    new NotBlank(array(
                        'message' => 'Wypełnij pole',
                    )),
            ), ),
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'form_client_account_change_password';
    }
}
