<?php

namespace Ls\UserBundle\Validators;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Doctrine\ORM\EntityManager;

/**
 * @Annotation
 */
class UserEmailExists extends Constraint
{
    public $message = 'Użytkownik z podanym adresem e-mail nie istnieje.';
}

class UserEmailExistsValidator extends ConstraintValidator
{
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function validate($value, Constraint $constraint)
    {
        if (!$this->userEmailExists($value)) {
            $this->context->addViolation($constraint->message);
        }
    }

    private function userEmailExists($email)
    {
        $usersCount = $this->entityManager
                ->createQueryBuilder()
                ->select('count(u.id)')
                ->from('LsUserBundle:User', 'u')
                ->where('u.email = :email')
                ->setParameter('email', $email)
                ->getQuery()
                ->getSingleScalarResult();

        return $usersCount > 0;
    }
}
