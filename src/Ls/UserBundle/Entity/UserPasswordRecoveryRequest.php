<?php

namespace Ls\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="user_password_recovery_request")
 */
class UserPasswordRecoveryRequest
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Ls\UserBundle\Entity\User"
     * )
     * @ORM\JoinColumn(
     *     name="user_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     *
     * @var \Ls\UserBundle\Entity\User
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=40)
     *
     * @var string
     */
    private $code;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $expirationDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $dateOfUse;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $createdAt;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->code = sha1(uniqid(null, true));
        $this->createdAt = new \DateTimeImmutable();
        $this->expirationDate = $this->createdAt->add(new \DateInterval('PT30M'));
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code.
     *
     * @param string $code
     *
     * @return UserPasswordRecoveryRequest
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set expirationDate.
     *
     * @param \DateTime $expirationDate
     *
     * @return UserPasswordRecoveryRequest
     */
    public function setExpirationDate($expirationDate)
    {
        $this->expirationDate = $expirationDate;

        return $this;
    }

    /**
     * Get expirationDate.
     *
     * @return \DateTime
     */
    public function getExpirationDate()
    {
        return $this->expirationDate;
    }

    /**
     * Set dateOfUse.
     *
     * @param \DateTime $dateOfUse
     *
     * @return UserPasswordRecoveryRequest
     */
    public function setDateOfUse($dateOfUse)
    {
        $this->dateOfUse = $dateOfUse;

        return $this;
    }

    /**
     * Get dateOfUse.
     *
     * @return \DateTime
     */
    public function getDateOfUse()
    {
        return $this->dateOfUse;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return UserPasswordRecoveryRequest
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set user.
     *
     * @param \Ls\UserBundle\Entity\User $user
     *
     * @return UserPasswordRecoveryRequest
     */
    public function setUser(\Ls\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \Ls\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
