<?php

namespace Ls\UserBundle\Service;

use Knp\Menu\ItemInterface;
use Ls\CoreBundle\Helper\AdminBlock;
use Ls\CoreBundle\Helper\AdminLink;
use Ls\CoreBundle\Helper\AdminRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AdminService
{
    private $container;
    private $authorizationChecker;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->authorizationChecker = $container->get('security.authorization_checker');
    }

    public function addToMenu(ItemInterface $parent, $route, $set)
    {
        if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $users = $parent->addChild('Użytkownicy', array(
                'route' => 'ls_admin_user',
            ));
        } else {
            $accountEdit = $parent->addChild('Edytuj konto', array(
                'route' => 'ls_user_client_account_edit_admin',
            ));
            $changePassword = $parent->addChild('Zmień hasło', array(
                'route' => 'ls_user_client_account_change_password_admin',
            ));
        }

        $current_set = true;

        if ($set) {
            return $current_set;
        }

        switch ($route) {
            case 'ls_admin_user':
            case 'ls_admin_user_new':
            case 'ls_admin_user_edit':
            case 'ls_admin_user_batch':
                if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
                    $users->setCurrent(true);
                }
                break;
            case 'ls_user_client_account_edit_admin':
                if (!$this->authorizationChecker->isGranted('ROLE_ADMIN')) {
                    $accountEdit->setCurrent(true);
                }
                break;
            case 'ls_user_client_account_change_password_admin':
                if (!$this->authorizationChecker->isGranted('ROLE_ADMIN')) {
                    $changePassword->setCurrent(true);
                }
                break;
            default:
                $current_set = false;
                break;
        }

        return $current_set;
    }

    public function addToDashboard(AdminBlock $parent)
    {
        $router = $this->container->get('router');

        if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $row = new AdminRow('Użytkownicy');
            $parent->addRow($row);

            $row->addLink(new AdminLink('Dodaj', 'glyphicon-plus', $router->generate('ls_admin_user_new')));
            $row->addLink(new AdminLink('Zarządzaj', 'glyphicon-list', $router->generate('ls_admin_user')));
        } else {
            $row = new AdminRow('Konto');
            $parent->addRow($row);

            $row->addLink(new AdminLink('Edytuj konto', 'glyphicon-plus', $router->generate('ls_user_client_account_edit_admin')));
            $row->addLink(new AdminLink('Zmień hasło', 'glyphicon-list', $router->generate('ls_user_client_account_change_password_admin')));
        }
    }
}
