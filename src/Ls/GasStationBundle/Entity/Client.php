<?php

namespace Ls\GasStationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="client")
 * @ORM\Entity
 */
class Client
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=10)
     *
     * @var string
     */
    private $nip;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $name;

    /**
     * @ORM\OneToMany(
     *   targetEntity="Ls\GasStationBundle\Entity\Order",
     *   mappedBy="client",
     *   cascade={"persist", "remove"}
     * )
     *
     * @var \Doctrine\Common\Collections\Collection
     */
    private $orders;

    /**
     * @ORM\OneToOne(
     *     targetEntity="Ls\UserBundle\Entity\User",
     *     mappedBy="client"
     * )
     * 
     * @var \Ls\UserBundle\Entity\User
     */
    private $user;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->orders = new ArrayCollection();
    }

    public function __toString()
    {
        return empty($this->getName()) ? '' : $this->getName();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nip.
     *
     * @param string $nip
     *
     * @return Client
     */
    public function setNip($nip)
    {
        $this->nip = $nip;

        return $this;
    }

    /**
     * Get nip.
     *
     * @return string
     */
    public function getNip()
    {
        return $this->nip;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Client
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return Client
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add order.
     *
     * @param \Ls\GasStationBundle\Entity\Order $order
     *
     * @return Client
     */
    public function addOrder(\Ls\GasStationBundle\Entity\Order $order)
    {
        $this->orders[] = $order;
        $order->setClient($this);

        return $this;
    }

    /**
     * Remove order.
     *
     * @param \Ls\GasStationBundle\Entity\Order $order
     */
    public function removeOrder(\Ls\GasStationBundle\Entity\Order $order)
    {
        $this->orders->removeElement($order);
    }

    /**
     * Get orders.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Set user.
     *
     * @param \Ls\UserBundle\Entity\User $user
     *
     * @return Client
     */
    public function setUser(\Ls\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \Ls\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Client
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
