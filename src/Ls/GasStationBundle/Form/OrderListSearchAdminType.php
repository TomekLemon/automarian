<?php

namespace Ls\GasStationBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;

class OrderListSearchAdminType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('clientName', null, array(
            'label' => 'Nazwa klienta',
        ));
        $builder->add('carRegistrationNumber', null, array(
            'label' => 'Rejestracja auta',
        ));
        $builder->add('orderDateFrom', DateTimeType::class, array(
            'label' => 'Data zamówienia od',
            'placeholder' => array(
                'year' => 'rok', 'month' => 'miesiąc', 'day' => 'dzień',
                'hour' => 'godzina', 'minute' => 'minuta', 'second' => 'sekunda',
            ),
        ));
        $builder->add('orderDateTo', DateTimeType::class, array(
            'label' => 'Data zamówienia do',
            'placeholder' => array(
                'year' => 'rok', 'month' => 'miesiąc', 'day' => 'dzień',
                'hour' => 'godzina', 'minute' => 'minuta', 'second' => 'sekunda',
            ),
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'form_order_list_search_admin';
    }
}
