<?php

namespace Ls\GasStationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Ls\GasStationBundle\Entity\Client;
use Ls\GasStationBundle\Form\ClientCreateAdminType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ClientAdminController extends Controller
{
    private $pager_limit_name = 'admin_gas_station_client_pager_limit';
    private $routes = array(
        'list' => 'ls_gas_station_client_list_admin',
        'create' => 'ls_gas_station_client_create_admin',
        'edit' => 'ls_gas_station_client_edit_admin',
        'delete' => 'ls_gas_station_client_delete_admin',
        'batch' => 'ls_gas_station_client_batch_admin',
    );

    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $session = $this->container->get('session');

        $page = $request->query->get('page', 1);
        if ($session->has($this->pager_limit_name)) {
            $limit = $session->get($this->pager_limit_name);
        } else {
            $limit = 15;
            $session->set($this->pager_limit_name, $limit);
        }

        $query = $em->createQueryBuilder()
            ->select('partial e.{id, name, nip, createdAt}, COUNT(o.id) as ordersCount')
            ->from('LsGasStationBundle:Client', 'e')
            ->leftJoin('e.orders', 'o')
            ->groupBy('e.id')
            ->getQuery();

        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $query,
            $page,
            $limit,
            array(
                'defaultSortFieldName' => 'e.name',
                'defaultSortDirection' => 'asc',
            )
        );
        $entities->setTemplate('LsCoreBundle:Backend:paginator.html.twig');

        if ($page > $entities->getPageCount() && $entities->getPageCount() > 0) {
            return $this->redirect($this->generateUrl($this->routes['list']));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Klienci', $this->get('router')->generate($this->routes['list']));

        return $this->render('LsGasStationBundle:Admin\Client:index.html.twig', array(
            'page' => $page,
            'limit' => $limit,
            'entities' => $entities,
        ));
    }

    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = new Client();

        $form = $this->createForm(ClientCreateAdminType::class, $entity, array(
            'action' => $this->generateUrl($this->routes['create']),
            'method' => 'POST',
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Zapisz'));
        $form->add('submit_and_list', SubmitType::class, array('label' => 'Zapisz i wróć na listę'));
        $form->add('submit_and_new', SubmitType::class, array('label' => 'Zapisz i dodaj następny'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Dodanie klienta zakończone sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl($this->routes['edit'], array('id' => $entity->getId())));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl($this->routes['list']));
            }
            if ($form->get('submit_and_new')->isClicked()) {
                return $this->redirect($this->generateUrl($this->routes['create']));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Klienci', $this->get('router')->generate($this->routes['list']));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate($this->routes['create']));

        return $this->render('LsGasStationBundle:Admin\Client:new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function editAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('LsGasStationBundle:Client')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Client entity.');
        }

        $form = $this->createForm(ClientCreateAdminType::class, $entity, array(
            'action' => $this->generateUrl($this->routes['edit'], array('id' => $entity->getId())),
            'method' => 'POST',
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Zapisz zmiany'));
        $form->add('submit_and_list', SubmitType::class, array('label' => 'Zapisz zmiany i zamknij'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Aktualizacja klienta zakończona sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl($this->routes['edit'], array('id' => $entity->getId())));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl($this->routes['list']));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Klienci', $this->get('router')->generate($this->routes['list']));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate($this->routes['edit'], array('id' => $entity->getId())));

        return $this->render('LsGasStationBundle:Admin\Client:edit.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('LsGasStationBundle:Client')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Client entity.');
        }

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add('success', 'Usunięcie klienta zakończone sukcesem.');

        return new Response('OK');
    }

    public function batchAction(Request $request)
    {
        $ids = $request->request->get('ids');
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = 'Czy na pewno chcesz ';
            switch ($action) {
                case 'delete':
                    $message .= 'usunąć ';
                    break;
            }
            $message .= $elements.' ';
            switch ($elements) {
                case 1:
                    $message .= 'element?';
                    break;
                case 2:
                case 3:
                case 4:
                    $message .= 'elementy?';
                    break;
                default:
                    $message .= 'elementów?';
                    break;
            }

            $breadcrumbs = $this->get('white_october_breadcrumbs');
            $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
            $breadcrumbs->addItem('Klienci', $this->get('router')->generate($this->routes['list']));
            $breadcrumbs->addItem('Potwierdzenie', $this->get('router')->generate($this->routes['batch']));

            return $this->render('LsGasStationBundle:Admin\Client:batch.html.twig', array(
                'message' => $message,
                'action' => $action,
                'ids' => implode(',', $ids),
            ));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');

            return $this->redirect($this->generateUrl($this->routes['list']));
        }
    }

    public function batchExecuteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $ids = explode(',', $request->request->get('ids'));
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = '';
            switch ($action) {
                case 'delete':
                    $message .= 'Usunięcie ';
                    $qb = $em->createQueryBuilder();
                    $query = $qb->select('e')
                        ->from('LsGasStationBundle:Client', 'e')
                        ->add('where', $qb->expr()->in('e.id', $ids))
                        ->getQuery();

                    $iterableResult = $query->iterate();
                    while (($row = $iterableResult->next()) !== false) {
                        $em->remove($row[0]);
                        $em->flush();
                    }

                    break;
            }
            $message .= $elements.' ';
            switch ($elements) {
                case 1:
                    $message .= 'elementu ';
                    break;
                default:
                    $message .= 'elementów ';
                    break;
            }
            $message .= 'zakończone sukcesem ';

            $this->get('session')->getFlashBag()->add('success', $message);

            return $this->redirect($this->generateUrl($this->routes['list']));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');

            return $this->redirect($this->generateUrl($this->routes['list']));
        }
    }

    public function setLimitAction(Request $request)
    {
        $session = $this->container->get('session');

        $limit = $request->request->get('limit');
        $session->set($this->pager_limit_name, $limit);

        return new Response('OK');
    }
}
