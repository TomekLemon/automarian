# Doctrine Migration File Generated on 2016-05-17 17:24:38

# Version 20160517171532
ALTER TABLE client ADD name VARCHAR(255) DEFAULT NULL, DROP full_name;
ALTER TABLE `order` ADD driver_full_name VARCHAR(255) DEFAULT NULL;
INSERT INTO migration_versions (version) VALUES ('20160517171532');
